/*global module:false*/
module.exports = function(grunt) {

  // Project configuration.
  grunt.initConfig({
    // Metadata.
    pkg: grunt.file.readJSON('package.json'),
    banner: '/*! <%= pkg.title || pkg.name %> - v<%= pkg.version %> - ' +
      '<%= grunt.template.today("yyyy-mm-dd") %>\n' +
      '<%= pkg.homepage ? "* " + pkg.homepage + "\\n" : "" %>' +
      '* Designed and coded by <%= pkg.author %> - <%= pkg.url %>\n' +
      '* <%= grunt.template.today("yyyy") %> <%= pkg.author.name %>*/\n',
    src: 'src',
    dist: 'dist',
    // Task configuration.
    clean: {
      dist: {
        src: ['<%= dist %>/']
      },
      production: {
        src: ['<%= dist %>/js/<%= pkg.name %>.js', '<%= dist %>/js/includes.js']
      },
      img: {
        src: ['<%= dist %>/img/']
      }
    },
    copy: {
      dist: {
        expand: true,
        cwd: './<%= src %>/assets/',
        src: ['.*', '*.*'],
        dest: '<%= dist %>/'
      },
      fonts: {
        expand: true,
        cwd: './<%= src %>/assets/',
        src: 'fonts/**/*',
        dest: '<%= dist %>/'
      },
      vendor: {
        expand: true,
        cwd: './<%= src %>/assets/js/',
        src: 'vendor/**/*',
        dest: '<%= dist %>/js/'
      },
      plugins: {
        expand: true,
        cwd: './<%= src %>/assets/js/',
        src: 'plugins/**/*',
        dest: '<%= dist %>/js/'
      },
      img: {
        expand: true,
        cwd: './<%= src %>/assets/',
        src: 'img/**/*',
        dest: '<%= dist %>/'
      }
    },
    assemble: {
      options: {
        flatten: true,
        engine: 'handlebars',
        assets: '<%= src %>/assets',
        layoutdir: '<%= src %>/layouts',
        layout: 'default.hbs',
        data: ['data/*.{json,yml}'],
        partials: ['<%= src %>/partials/**/*.hbs' ]
      },
      production: {
        options: {
          name: '<%= pkg.name %>',
          production: true
        },
        files: [{ 
          expand: true, 
          cwd: '<%= src %>/', 
          src: ['*.hbs'], 
          dest: '<%= dist %>/' 
        }]
      },
      development: {
        options: {
          name: '<%= pkg.name %>',
          production: false
        },
        files: [{ 
          expand: true, 
          cwd: '<%= src %>/', 
          src: ['*.hbs'], 
          dest: '<%= dist %>/' 
        }]
      }
    },
    concat: {
      options: {
        banner: '<%= banner %>',
        stripBanners: true
      },
      dist: {
        src: ['<%= src %>/assets/js/main.js'],
        dest: '<%= dist %>/js/<%= pkg.name %>.js'
      },
      includes: {
        src: ['<%= src %>/assets/js/includes/**/*.js'],
        dest: '<%= dist %>/js/includes.js'
      }
    },
    uglify: {
      options: {
        banner: '<%= banner %>'
      },
      dist: {
        src: '<%= concat.dist.dest %>',
        dest: '<%= dist %>/js/<%= pkg.name %>.min.js'
      },
      includes: {
        src: '<%= concat.includes.dest %>',
        dest: '<%= dist %>/js/includes.min.js'
      }
    },
    less: {
      production: {
        options: {
          banner: '<%= banner %>',
          stripBanners: true,
          cleancss: true
        },
        src: '<%= src %>/assets/less/*.less',
        dest: '<%= dist %>/css/<%= pkg.name %>.min.css'
      },
      development: {
        options: {
          banner: '<%= banner %>',
          stripBanners: true,
          cleancss: false
        },
        src: '<%= src %>/assets/less/*.less',
        dest: '<%= dist %>/css/<%= pkg.name %>.css'
      }
    },
    imagemin: {
      dist: {
        options: {
          optimizationLevel: 7
        },
        files: [{
          expand: true,
          cwd: '<%= src %>/assets/img/',
          src: ['**/*.{png,jpg,gif}'],
          dest: '<%= dist %>/img/'
        }]
      }
    },
    watch: {
      less: {
        files: '<%= src %>/assets/less/**/*',
        tasks: 'less',
        options: {
          livereload: true
        }
      },
      assemble: {
        files: '<%= src %>/**/*.hbs',
        tasks: 'assemble:development',
        options: {
          livereload: true
        } 
      },
      js: {
        files: '<%= src %>/assets/js/**/*',
        tasks: ['concat'],
        options: {
          livereload: true
        } 
      },
      copy: {
        files: '<%= src %>/assets/img/*',
        tasks: ['clean:img', 'copy:img'],
        options: {
          livereload: true
        } 
      },
      copy_fonts: {
        files: '<%= src %>/assets/fonts/**/*',
        tasks: ['copy:fonts'],
        options: {
          livereload: true
        } 
      }
    }
  });

  // These plugins provide necessary tasks.
  grunt.loadNpmTasks('grunt-contrib-concat');
  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-contrib-less');
  grunt.loadNpmTasks('grunt-contrib-copy');
  grunt.loadNpmTasks('grunt-contrib-clean');
  grunt.loadNpmTasks('grunt-contrib-imagemin');
  grunt.loadNpmTasks('assemble');
  grunt.loadNpmTasks('grunt-contrib-watch');

  // Default task.
  grunt.registerTask('default', ['clean:dist', 'copy', 'imagemin', 'assemble:development', 'concat', 'less:development' ]);
  grunt.registerTask('production', ['clean:dist', 'copy', 'imagemin', 'assemble:production', 'concat', 'less:production', 'uglify', 'clean:production']);
};
